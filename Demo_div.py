# -*- coding: utf-8 -*-
"""
Created on Mon Jan 20 22:57:12 2020

@author: RND

usage: python Demo_Script_With_arg.py
"""



import sys
sys.path.append("./PackageAndModules")
import operations.operation_basique as op_basique




if __name__ == "__main__":
    a = 7
    b = 2
    result = op_basique.division(a, b)
    print(result)

    f = open("./Output/result.txt", "a")
    f.write("Result Div:" + str(result))
    f.write('\n')
    
