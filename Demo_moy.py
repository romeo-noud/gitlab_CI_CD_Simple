# -*- coding: utf-8 -*-
"""
Created on Mon Jan 20 22:57:12 2020

@author: RND

usage: python Demo_Script_With_arg.py
"""



import sys
sys.path.append("./PackageAndModules")
import operations.operation_statistique as op_statistique




if __name__ == "__main__":
	number_list = [1, 4, 8, 9, 34]
	result = op_statistique.moyenne(number_list)
	print(result)

	f = open("./Output/result.txt", "a")
	f.write("Result Moy:" + str(result))
	f.write('\n')
    
